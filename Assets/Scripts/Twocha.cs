﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Twocha : RandomEvent
{
    TwochaType m_twochaType = TwochaType.Common;
    Vector2 m_position = Vector2.zero;
    int m_investigationCost = 0;
    int m_minWindSpeed = 0;
    int damage = 0;

    public void Initialize(TwochaType twochaType, Vector2 position, int cost)
    {
        m_twochaType = twochaType;
        m_position = position;
        m_investigationCost = cost;
    }

    void DoDamage()
    {
        switch (m_twochaType)
        {
            case TwochaType.Smoke:
                //ApplicationContainer.Instance.GridController.GetGrid(m_position).GetDamage(Random.Range(damage-5, damage+5));
                break;
            case TwochaType.Toxic:
                //ApplicationContainer.Instance.GridController.GetGrid(m_position).GetDamage(Random.Range(damage-5, damage+5));
                break;
        }
    }

    void StartWindMoveCycle(Vector2 endPosition)
    {
        //m_position =  endPosition;
        //transform.DOMove();
    }
}
