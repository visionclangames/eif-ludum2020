﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    [SerializeField] GameObject m_gridPrefab = null;

    GridElement[,] m_gridElements;
    GridElement m_currentSelection = null;

    private void Start()
    {
        //PLACEHOLDER
        m_gridElements = new GridElement[10, 10];
        for(int i = 0; i<10; i++)
        {
            for(int  j = 0;j<10; j++)
            {
                AddGridItem(i, j);
               
            }
        }
    }

    public void setCurrentSelection(Vector2Int coords)
    {if (coords.x >= 0 && coords.y >= 0)
        {
            m_currentSelection = m_gridElements[coords.x, coords.y];
        }
        else
        {
            m_currentSelection = null;
        }
    }

    public GridElement getCurrentSelection()
    {
        Debug.Log("kek");
        if (m_currentSelection == null){
            return null;
        }
        else {
            return m_currentSelection;
        }
    }

    void AddGridItem(int xCoord, int yCoord)
    {
     GridElement a = Instantiate(m_gridPrefab, new Vector3(xCoord * 10, 0, yCoord * 10), Quaternion.identity).GetComponent<GridElement>();
        a.Coords = new Vector2Int(xCoord, yCoord);
        m_gridElements[xCoord, yCoord] = a;
    }
}
