﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridElement : MonoBehaviour
{
    public Vector2Int Coords { get; set; } = Vector2Int.zero;

    private MeshRenderer m_mat;

    private void Start()
    {
        m_mat = GetComponent<MeshRenderer>();
    }

    private void OnMouseDown()
    {
        if (ApplicationContainer.Instance.GridManager.getCurrentSelection() != null) {
            ApplicationContainer.Instance.GridManager.getCurrentSelection().SetColor(Color.white);
        }
        if (ApplicationContainer.Instance.GridManager.getCurrentSelection() != this)
        {
            ApplicationContainer.Instance.GridManager.setCurrentSelection(Coords);
            ApplicationContainer.Instance.GridManager.getCurrentSelection().SetColor(Color.black);
        }
        else
        {
            ApplicationContainer.Instance.GridManager.getCurrentSelection().SetColor(Color.white);
            ApplicationContainer.Instance.GridManager.setCurrentSelection(new Vector2Int (-1, -1));
        }
    }

    private void OnMouseEnter()
    {
        if (ApplicationContainer.Instance.GridManager.getCurrentSelection() != null)
        {
            if (ApplicationContainer.Instance.GridManager.getCurrentSelection() != this)
            {
                SetColor(Color.blue);
            }
        }
        else
        {
            SetColor(Color.blue);
        }
    }
    private void OnMouseExit()
    {
        if (ApplicationContainer.Instance.GridManager.getCurrentSelection() != null)
        {
            if (ApplicationContainer.Instance.GridManager.getCurrentSelection() != this)
            {
                SetColor(Color.white);
            }
        }
        else
        {
            SetColor(Color.white);
        }
    }
    public void SetColor(Color color)
    {
        m_mat.material.color = color;
    }
}
