﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomEventHolder : MonoBehaviour
{
    [SerializeField] GameObject m_Twocha = null;
    List<RandomEvent> m_foundRandomEvents = new List<RandomEvent>();
    List<RandomEvent> m_hiddenRandomEvents = new List<RandomEvent>();

    private void Start()
    {
        Twocha a = Instantiate(m_Twocha, new Vector3(1*10, 5, 2*10), Quaternion.identity).GetComponent<Twocha>();
        a.Initialize(TwochaType.Toxic, new Vector2(1, 2),15);
        m_hiddenRandomEvents.Add(a);
        a = Instantiate(m_Twocha, new Vector3(5 * 10, 5, 3 * 10), Quaternion.identity).GetComponent<Twocha>();
        a.Initialize(TwochaType.Toxic, new Vector2(5, 3), 15);
        m_hiddenRandomEvents.Add(a);
        a = Instantiate(m_Twocha, new Vector3(7 * 10, 5, 5 * 10), Quaternion.identity).GetComponent<Twocha>();
        a.Initialize(TwochaType.Toxic, new Vector2(7, 5), 15);
        m_hiddenRandomEvents.Add(a);

    }
}
