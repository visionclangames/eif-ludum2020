﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationContainer : MonoBehaviour
{
    public static ApplicationContainer Instance;

    #region Variable
    [SerializeField] public UIController UIController;
    [SerializeField] public GridManager GridManager;
    #endregion

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            return;
        }
    }


}
